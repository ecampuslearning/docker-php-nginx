#!/usr/bin/env bats

setup() {
    load 'test_helper/common-setup'
    _common_setup
}

@test "php does not return any warnings" {
  run docker run ${container} php -m

  assert_success
  refute_output 'PHP Warning'
  refute_output 'PHP Notice'
}

@test "php should have memory limit set to 256M" {
  run bash -c "docker run --rm ${container} php -i | grep memory_limit"

  assert_success
  assert_output "memory_limit => 256M => 256M"
}

@test "php is set to have asia/tokyo as its timezone " {
  run bash -c "docker run --rm ${container} php -i | grep date.timezone"

  assert_success
  assert_output "date.timezone => Asia/Tokyo => Asia/Tokyo"
}

@test "php has short open tag enabled" {
  run bash -c "docker run ${container} php -i | grep -w short_open_tag"

  assert_success
  assert_output "short_open_tag => On => On"
}
