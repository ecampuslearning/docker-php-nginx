SHELL=/bin/bash

REG_HOST=registry.gitlab.com
REG_USER=stelgenhof
REG_PROJECT=docker-php-nginx
REG_REPO=$(REG_HOST)/$(REG_USER)/$(REG_PROJECT)

DOCKER_BUILD_CONTEXT=.
DOCKER_FILE=Dockerfile

IMAGES=$(shell find -maxdepth 1 -type d -name '[0-9]*' | xargs basename -a)

NO_COLOR=\033[0m
OK_COLOR=\033[0;32m
INFO_COLOR=\033[0;35m
WARN_COLOR=\033[0;33m

build_args := --build-arg BUILD_DATE=$(shell date -u +"%Y-%m-%dT%H:%M:%SZ") \
              --build-arg VCS_REF=$(shell git rev-parse --short HEAD)

.DEFAULT: help
all: help

build: build-7.3 build-7.4 build-8.0 build-8.1 build-8.2 ## Build all base images
build-quick: ## Build all base images without pulling a newer version
	make build cache="" pull=""

test: test-7.3 test-7.4 test-8.0 test-8.1 test-8.2 ## Test all images
push: push-7.3 push-7.4 push-8.0 push-8.1 push-8.2 ## Push all images
clean: clean-7.3 clean-7.4 clean-8.0 clean-8.1 clean-8.2 ## Clean up all images

build-%: cache ?= --no-cache
build-%: pull ?= --pull
build-%: ## Build a base image
	@if [ ! -d "./$*" ]; then printf '$(WARN_COLOR) > The "%s" image directory does not exist <\n$(NO_COLOR)' $* && exit 1; fi

	@printf '$(INFO_COLOR)Building the "$*" image...\n$(NO_COLOR)' $*
	@docker build ${build_args} -t $(REG_REPO):php$(subst .,,$*) -f $*/$(DOCKER_FILE) $*

test-%: ## Test a specific image
	@printf '$(INFO_COLOR)Testing image "$*"...\n$(NO_COLOR)' $*
	@echo -e '\n > verifying base image settings'
	@container=$(REG_REPO):php$(subst .,,$*) test/bats/bin/bats ./test/image.bats ./test/os.bats ./test/php.bats

	@echo -e '> verifying image specific settings'
	@container=$(REG_REPO):php$(subst .,,$*) test/bats/bin/bats ./$*/tests.bats

clean-%: ## Clean up a specific image
	@printf '$(INFO_COLOR)Cleaning up image "$*"...\n$(NO_COLOR)' $*
	@docker rmi $$(docker images -q $(REG_REPO):php$(subst .,,$*)) 2>/dev/null || printf '$(WARN_COLOR) > No images with the name "%s" found <\n$(NO_COLOR)' $*

push-%: ## Push a specific image
	@printf '$(INFO_COLOR)Pushing image "$*"...\n$(NO_COLOR)' $*
	@docker push $(REG_REPO):php$(subst .,,$*)

help: ## Show this help.
	@echo -e "$(OK_COLOR)[ Docker PHP NGINX Image Builder ]$(NO_COLOR)"
	@echo ''
	@echo 'Usage:'
	@echo '  make <target>'
	@echo ''
	@echo 'Targets:'
	@grep -E '^[a-zA-Z0-9%_.-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}'

