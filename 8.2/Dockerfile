FROM php:8.2-fpm-alpine

ARG BUILD_DATE
ARG VCS_REF

LABEL org.opencontainers.image.authors="Sacha Telgenhof <me@sachatelgenhof.com" \
    org.opencontainers.image.title="docker-php-nginx" \
    org.opencontainers.image.description="Lightweight container with latest NGINX & PHP-FPM based on Alpine Linux" \
    org.opencontainers.image.url="https://gitlab.com/stelgenhof/docker-php-nginx" \
    org.opencontainers.image.revision=$VCS_REF \
    org.opencontainers.image.created=$BUILD_DATE

# Ensure to use HTTPS
RUN apk --update add ca-certificates

# Install system dependencies
RUN apk --no-cache add nginx supervisor curl git icu-dev zip libzip-dev

# Install PHP Extensions
RUN docker-php-ext-install -j$(nproc) bcmath calendar intl opcache zip

# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

# Configure nginx
COPY config/nginx.conf /etc/nginx/nginx.conf

# Remove default server definition
RUN rm /etc/nginx/http.d/default.conf

# Configure PHP-FPM
COPY config/fpm-pool.conf /etc/php82/php-fpm.d/www.conf
COPY config/php.ini /usr/local/etc/php/conf.d/99_custom.ini

# Configure supervisord
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf

# Setup document root
RUN mkdir -p /var/www/html

# Make sure files/folders needed by the processes are accessible when they run under the nobody user
RUN chown -R nobody.nobody /var/www/html && \
  chown -R nobody.nobody /run && \
  chown -R nobody.nobody /var/lib/nginx && \
  chown -R nobody.nobody /var/log/nginx

RUN mkdir /.composer && \
    chown -R nobody.nobody /.composer

# Switch to use a non-root user from here on
USER nobody

# Add application
WORKDIR /var/www/html

# Expose the port nginx is reachable on
EXPOSE 8080

# Let supervisord start nginx & php-fpm
CMD ["/usr/bin/supervisord", "-c", "/etc/supervisor/conf.d/supervisord.conf"]

# Configure a healthcheck to validate that everything is up&running
HEALTHCHECK --timeout=10s CMD curl --silent --fail http://127.0.0.1:8080/fpm-ping
